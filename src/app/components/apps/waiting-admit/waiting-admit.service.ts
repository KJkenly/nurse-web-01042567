import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WaitingAdmitService {

  pathPrefixLookup: any = `api-lookup/lookup`;
  pathPrefixDoctor: any = `api-doctor/doctor`;
  pathPrefixNurse: any = `api-nurse/nurse`;
  pathPrefixAuth: any = `api-auth/auth`;

  private axiosInstanceNurse = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixNurse}`
  });
  private axiosInstanceLookup = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  })

  constructor() {
    this.axiosInstanceLookup.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstanceLookup.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    })

    /////////////////////////
    this.axiosInstanceNurse.interceptors.request.use(config => {
        const token = sessionStorage.getItem('token');
        if (token) {
          config.headers['Authorization'] = `Bearer ${token}`;
        }
        return config;
      });
  
      this.axiosInstanceNurse.interceptors.response.use(response => {
        return response;
      }, error => {
        return Promise.reject(error);
      })
  }

  async getWard() {
    const url = `/ward`;
    return this.axiosInstanceLookup.get(url);
  }

  async getWardName(wardId: any) {
    const url = `/ward/${wardId}`;
    return this.axiosInstanceLookup.get(url);
  }

  async getBed(wardId: any) {
    const url = `/bed/${wardId}/ward`;
    return this.axiosInstanceLookup.get(url);
  }

  async getDoctor() {
    const url = `/doctor`;
    return this.axiosInstanceLookup.get(url);
  }

  
  async getAdmitActive(wardId: any, query: any, limit: any, offset: any) {
    const url = `/admit?axiosInstanceLookup=${wardId}&query=${query}&limit=${limit}&offset=${offset}`
    return await this.axiosInstanceLookup.get(url)
  }

  async getWaiting(limit: any, offset: any, dateStart: any, dateEnd: any) {
 // const url = `http://209.15.114.75/api-nurse/nurse/his-services/waiting-info?limit=20&offset=0&dateStart=${dateStart}&dateEnd=${dateEnd}`;
 console.log("dateStart="+dateStart+" | "+"dateEnd="+dateEnd);
  const url = `/his-services/waiting-info?limit=${limit}&offset=${offset}&dateStart=${dateStart}&dateEnd=${dateEnd}`
    return await this.axiosInstanceNurse.get(url)
  }
  async getWaitingInfo(an: any) {
    const url = `/his-services/waiting-info?an=${an}`
    return await this.axiosInstanceNurse.get(url)
  }

  async getReview(an: any) {
    const url = `/his-services/waiting-review?an=${an}`
    return await this.axiosInstanceNurse.get(url)
  }

  async getTreatement(an: any) {
    const url = `/his-services/waiting-treatement?an=${an}`
    return await this.axiosInstanceNurse.get(url)
  }

  async getPatientAllergy(an: any) {
    const url = `/his-services/waiting-patient-allergy?an=${an}`
    return await this.axiosInstanceNurse.get(url)
  }

  async getWaitingAdmit(an: any) {
    const url = `/his-services/waiting-admit?an=${an}`
    return await this.axiosInstanceNurse.get(url)
  }

  async saveRegister(data: object) {
    return await this.axiosInstanceNurse.post('/admit/upsert-admit', data)
  }
  async getAdmit(admit_id: any) {
    console.log(admit_id);
    const url = `/admit/${admit_id}`
    console.log(url);
    return await this.axiosInstanceLookup.get(url)
  }

  async changeWard(data: any, admit_id: any) {
    // const admit_id = data.admit_id;
    console.log(data);
    return await this.axiosInstanceNurse.put(`/utility/change-ward/${admit_id}`, data)
  }
  async changeBed(data: any, admit_id: any) {
    // const admit_id = data.admit_id;
    console.log(data);
    return await this.axiosInstanceNurse.put(`/utility/change-bed-type/${admit_id}`, data)
  }

  async changeDoctor(data: any, admit_id: any) {
    console.log(data);
    return await this.axiosInstanceNurse.put(`/admit/${admit_id}`, data)

  }

  async getNurseNote(admit_id: any) {
    console.log(admit_id);
    const url = `/nurse_note/${admit_id}`
    console.log(url);
    return await this.axiosInstanceLookup.get(url)
  }

  async getActivity() {
    const url = `/activity`
    console.log(url);
    return await this.axiosInstanceLookup.get(url)
  }

  async getEvaluate() {
    const url = `/evaluate`
    console.log(url);
    return await this.axiosInstanceLookup.get(url)
  }

  async saveNurseNote(data: object) {
    return await this.axiosInstanceNurse.post('/nurse_note', data)
  }

  async getNursePatient(an:any) {
    const url = `/patient/${an}`
    console.log(url);
    return await this.axiosInstanceNurse.get(url)
  }

  async getNurseinfo(admit_id:any) {
    const url = `/info/${admit_id}`
    console.log(url);
    return await this.axiosInstanceNurse.get(url)
  }

  async getNursevital_sign(admit_id:any) {
    const url = `/vital_sign/${admit_id}`
    console.log(url);
    return await this.axiosInstanceNurse.get(url)
  }
    //ken
    async saveNurseVitalSign(data: object) {
      return await this.axiosInstanceNurse.post('/vital_sign', data)
    }
    async getDatabedanddoc(admit_id:any) {
      const url = `/nurse_note/${admit_id}`
      console.log(url);
      return await this.axiosInstanceLookup.get(url)
    }
  async getdoctor_order(admit_id:any) {
    const url = `/doctor_order/${admit_id}`
    console.log(url);
    return await this.axiosInstanceNurse.get(url)
  }
  

  async getAdmitActive_all() {
    const url = `/admit/is_admit`
    return await this.axiosInstanceNurse.get(url)
  }

  async getorder(admit_id:any) {
    const url = `/doctor_order/order/${admit_id}`
    console.log(url);
    return await this.axiosInstanceNurse.get(url)
  }
}
