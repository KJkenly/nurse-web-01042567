import { Component, OnInit, LOCALE_ID,ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';

import {WaitingAdmitService } from './waiting-admit.service';

import * as _ from 'lodash';
import { DateTime } from 'luxon';

import { LookupService } from '../../../shared/lookup-service';

import { NgxSpinnerService } from 'ngx-spinner';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { MenuItem } from 'primeng/api';
import { Calendar } from 'primeng/calendar';
import { PrimeNGConfig } from 'primeng/api';


@Component({
  selector: 'app-waiting-admit',
  templateUrl: './waiting-admit.component.html',
  styleUrls: ['./waiting-admit.component.scss'],
  providers: [MessageService, ConfirmationService, { provide: LOCALE_ID, useValue: 'th' }],

})
export class WaitingAdmitComponent implements OnInit {
  selectedDate: Date = new Date;
  @ViewChild('calendar')
  calendar!: Calendar;
  th = {
    firstDayOfWeek: 1,
    dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุทธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
    dayNamesShort: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
    monthNames: [
      "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
      "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"
    ],
    monthNamesShort: [
      "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.",
      "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."
    ],
    today: 'Today',
    clear: 'Clear',
    dateFormat: 'dd/mm/yy',
    weekHeader: 'สัปดาห์'
  };
  queryParamsData: any;

  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  wardId: any;
  wards: any = [];
  isLoading: boolean = true;
  dateStart: any;// = new Date();
  dateEnd: any ;//= new Date();
  Admit_date: any;

  isVisible = false;
  isOkLoading = false;
  inputValue?: string;
  modalTitle: string = 'ระบุแพทย์เจ้าของไข้';
  selectedValue: any = null;
  selectedPatient:any;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private waitingAdmitService: WaitingAdmitService,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    public primengConfig: PrimeNGConfig,

  ) {
   
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log('params', this.queryParamsData);
    this.primengConfig.setTranslation(this.th);
  }



onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal(
        (event.target as HTMLInputElement).value,
        'contains'
    );
}

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
  ngOnInit() {

    // this.user_login_name  =  this.userProfileService.user_login_name;
    this.user_login_name = sessionStorage.getItem('userLoginName');
  //  console.log(this.user_login_name);
    this.getWard();
  }

  listPatients :any = [];
  async getWard() {
    try {
      const response: AxiosResponse = await this.waitingAdmitService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      await this.getList(0);
      this.isLoading = false;
    } catch (error) {
      console.log(error);
    }
  }
    response: any;
    response_admit:any ;

  async getList(cond: any) {
  //  console.log('getList');
  
    this.spinner.show();
    let startDate = DateTime.fromJSDate(this.Admit_date).setLocale('en').toFormat('yyyy-MM-dd');
    let endDate = DateTime.fromJSDate(this.Admit_date).setLocale('en').toFormat('yyyy-MM-dd');

    try {
      const _limit = this.pageSize;
      const _offset = this.offset;

      if(cond== 0){
   

          //  console.log(startDate);
          this.response= await this.waitingAdmitService.getWaiting(_limit, _offset, startDate, endDate);
  //  const response_admit = await this.waitingAdmitService.getWaiting(_limit, _offset, startDate, endDate);
          this.response_admit= await this.waitingAdmitService.getAdmitActive_all();    
   
  }
  //    console.log("getWaiting="+response);
   //   console.log("getAdmitActive_all="+response_admit);
      /* ตัดคนที่ถูก  admit ward ออก */
      let data: any =  this.response.data;
      // let dataadmit: any=[];
      data.forEach( (item:any) => {
        item.is_admit = false;
        let _data :any  =  this.response_admit.data.data.find((x:any) =>x.an == item.an);
        if(_data){
          item.is_admit = true;
        } else {
          item.is_admit = false;
        }
      })
      console.log('cond',cond);
    

      if(cond==0){
        data = data.filter((item:any) => item.is_admit == false );
        console.log('data1',data) 

      }else{
    //    data = data.filter((item:any) => item.is_admit == false,(item:any) => item.admit_date == startDate);
    //    console.log('data2',data) 

      }

   

      this.total = data.length || 1
      this.dataSet = data.map((v: any) => {
     
        let _date = new Date(v.admit_date);
        // แปลงวันที่เป็น ISO 8601 และแยกส่วนของวันที่ออกมา
        let _time = _date.getTime() + (7*60*60*1000);
        v.search_date = new Date(_time).toISOString().split('T')[0];

        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
      //  const time = v.admit_time ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.TIME_24_SIMPLE) : '';
    //  const time = v.admit_time ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.TIME_24_WITH_SHORT_OFFSET) : '';
      const time = v.admit_time
      ? DateTime.fromFormat(v.admit_time, 'HH:mm:ss').toFormat(
          'HH:mm'
      )
      : '';
        v.admit_time = time;
        v.admit_date = date;
        // v.admit_time = time;
        return v;
      });
      this.listPatients =this.dataSet;
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    } catch (error: any) {
      console.log(error);
      this.hideSpinner();
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด !',
        detail: 'กรูณาตรวจสอบ'+ error,
    });
    }
  }
  navigateRegister(datas:any){
    console.log('navigateRegister');
    
    console.log('an:',datas.an);   
    let an = {'an':datas.an};
    let jsonString = JSON.stringify(an);

    this.router.navigate(['/register'], { queryParams: { data: jsonString } });
  }

  onDateSelect(event:any){   
    if(event != null ){

    console.log('an:',event);  
    let date = new Date(event);
    let _time = date.getTime() + (7*60*60*1000);
    let search_date = new Date(_time).toISOString().split('T')[0];
    console.log('an:',search_date);  
    console.log('listPatients:',this.listPatients);  
    this.dataSet= this.listPatients.filter(
    (x:any) => x.search_date === search_date
    );
   // this.dataSet =data;
    }else{

      this.dataSet = this.listPatients;
    }
}

}
