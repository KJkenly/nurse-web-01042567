import { NgModule,CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DataViewModule } from 'primeng/dataview';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { DividerModule } from 'primeng/divider';
import { AvatarModule } from 'primeng/avatar';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { TagModule } from 'primeng/tag';
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/chips';
import { DialogModule } from 'primeng/dialog';
import { SidebarModule } from 'primeng/sidebar';
import { MenuModule } from 'primeng/menu';
import { PanelModule } from 'primeng/panel';
import { SplitterModule } from 'primeng/splitter';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { RippleModule } from 'primeng/ripple';
import { InputTextareaModule } from 'primeng/inputtextarea';
import {RadioButtonModule} from 'primeng/radiobutton';
import { SpeedDialModule } from 'primeng/speeddial';
import {SharedModule} from '../../../shared/sharedModule'
import {NgxSpinnerModule} from 'ngx-spinner'
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TabViewModule } from 'primeng/tabview';
import { StyleClassModule } from 'primeng/styleclass';
import { NgApexchartsModule } from "ng-apexcharts";

import { NurseNoteRoutingModule } from './nurse-note-routing.module';
import { NurseNoteComponent } from './nurse-note.component';
import { OrdersComponent } from './orders/orders.component';
import { NotesComponent } from './notes/notes.component';
import { VitalSignComponent } from './vital-sign/vital-sign.component';

import { AccordionModule } from 'primeng/accordion';


@NgModule({
  declarations: [
    NurseNoteComponent,
    OrdersComponent,
    NotesComponent,
    VitalSignComponent
  ],
 
  imports: [
    CommonModule,    
    AccordionModule,
    CommonModule,SharedModule,FormsModule,ReactiveFormsModule,MenuModule,DialogModule,SidebarModule,ChipsModule,SpeedDialModule,NgxSpinnerModule,AutoCompleteModule,
    NurseNoteRoutingModule,ConfirmPopupModule,ConfirmDialogModule,ToastModule,TableModule,TagModule,MessagesModule,MessageModule,
    DropdownModule,ButtonModule,AvatarModule,DividerModule,CalendarModule,TabViewModule,NgApexchartsModule,InputTextModule,RippleModule,StyleClassModule,CheckboxModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA
  ]
})
export class NurseNoteModule { }

