import { Component, Input, ViewChild } from '@angular/core';
import { Location } from '@angular/common';

import { Router, ActivatedRoute } from '@angular/router';
import { VitalSignService } from './vital-sign.service';
import { DateTime } from 'luxon';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AxiosResponse } from 'axios';

import {
  ChartComponent, ApexAxisChartSeries, ApexChart, ApexXAxis, ApexDataLabels, ApexStroke, ApexMarkers, ApexYAxis, ApexGrid, ApexTitleSubtitle, ApexLegend,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  markers: ApexMarkers;
  colors: string[];
  yaxis: ApexYAxis;
  grid: ApexGrid;
  legend: ApexLegend;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-vital-sign',
  templateUrl: './vital-sign.component.html',
  styleUrls: ['./vital-sign.component.scss']
})
export class VitalSignComponent {
  @ViewChild('chart', { static: true }) chart: any;
  @Input() parentData: any;

  public chartOptions: any;
  basicBarChart: any;
  roundnurse: any[] = [];
  roundnurse1: any[] = ['2', '6', '10', '14', '18', '22'];
  roundnurse3: any[] = [{ name: '2' }, { name: '6' }, { name: '10' }, { name: '14' }, { name: '18' }, { name: '22' }];
  roundnurse2: any[] = ['2'];
  selectedRoundNurse: any;
  //public
  doctors: any = [];
  doctorname: any;
  //
  //checked = true;
  isVisible = false;
  isVisibleVS = false;
  isVisibleVSEdit = false;
  isVisibleconsult = false;
  isVisibleorder = false;
  isVisiblenurse = false;
  queryParamsData: any;

  itemPatientInfo: any;
  anFromParent: any;
  sessionPatientInfo: any;
  //ken writing
  sessionListnursenote: any;
  listnursenote: any;
  itemActivityInfo: any;
  itemEvaluate: any;
  activity_checked: any = [];
  evaluate_checked: any = [];
  problem_list: any;
  itemNursenote: any;
  getproblem_list: any;
  countrowdata: any;
  itemNursenoteDate: any;
  itemNursenoteTime: any;
  isSaved = false;
  //แสดงข้อมูลด้านบนสุดของแต่ละแท็บ
  topbed: any;
  //Patient info
  address: any;
  admit_id: any;
  age: any;
  an: any;
  blood_group: any;
  cid: any;
  citizenship: any;
  create_by: any;
  create_date: any;
  dob: any;
  fname: any;
  gender: any;
  hn: any;
  id: any;
  inscl: any;
  inscl_name: any;
  insurance_id: any;
  is_active: any;
  is_pregnant: any;
  lname: any;
  married_status: any;
  modify_by: any;
  modify_date: any;
  nationality: any;
  occupation: any;
  phone: any;
  reference_person_address: any;
  reference_person_name: any;
  reference_person_phone: any;
  reference_person_relationship: any;
  religion: any;
  title: any;

  //Nurse V/S info
  body_height: any;
  body_temperature: any;
  body_weight: any;
  chief_complaint: any;
  diatolic_blood_pressure: any;
  eye_score: any;
  movement_score: any;
  oxygen_sat: any;
  past_history: any;
  physical_exam: any;
  present_illness: any;
  pulse_rate: any;
  respiratory_rate: any;
  systolic_blood_pressure: any;
  verbal_score: any;
  waist: any;
  //ken VS input
  rr: any;
  bp: any;
  bw: any;
  bt: any;
  pr: any;
  sp: any;
  dp: any;
  painscore: any;
  oralfluide: any;
  parenterat: any;
  urineout: any;
  emesis: any;
  drainage: any;
  aspiration: any;
  stools: any;
  urine: any;
  medications: any;
  round_time: any;

  // Nurse Note
  NurseNote: any = [];
  // Doctor Order
  doctorOrder: any = [];
  //Nurse Vital Sign
  NurseVitalSign: any = [];
  NurseVitalSignLast: any = [];
  //ตัวแปรดึงข้อมูลจากกราฟ
  startdate!: Date;
  nextdate!: Date;
  //แกน x/y
  temperature: any = [];
  pulse: any = [];
  vitalSignRound: any = [];
  vitalSignRoundGroup: any = [];

  dataxnamex: any = [];
  datay: any = [];
  datetop: any = [];

  isVisibleVitalSign: boolean = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private vitalSignService: VitalSignService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private spinner: NgxSpinnerService,
    private location: Location


  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    // console.log('queryParamsData : ', this.queryParamsData);
    this.apexChartLine();
  }
  apexChartLine() {
    this.chartOptions = {
      series: [
        {
          name: 'Temperature',
          data: this.temperature,
          //   data:[
          //     "37.0",
          //     "37.8",
          //     "36.3",
          //     "37.9",
          //     "38.5",
          //     "37.0",
          //     "37.5",
          //     "37.0",
          //     "37.8",
          //     "36.3",
          //     "37.9",
          //     "38.5",
          //     "37.0",
          //     "37.5"
          // ],
          type: 'line',
        },
        {
          name: 'Pulse Rate',
          data: this.pulse,
          //   data: [
          //     82,
          //     80,
          //     78,
          //     86,
          //     89,
          //     85,
          //     80,
          //     82,
          //     80,
          //     78,
          //     86,
          //     89,
          //     85,
          //     80,
          // ],
          type: 'line',
          yAxis: 1, // ให้ข้อมูล Pulse Rate ใช้แกน Y ที่ 2
        },
      ],
      chart: {
        height: 300,
        width: '900px',
        type: 'line',
        dropShadow: {
          enabled: true,
          color: '#000',
          top: 18,
          left: 7,
          blur: 10,
          opacity: 0.2,
        },
        toolbar: {
          show: false,
        },
      },
      colors: ['#0066ff', '#ff0000'],
      dataLabels: {
        enabled: true,
      },
      stroke: {
        curve: 'smooth',
        width: 2,
      },
      title: {
        text: 'Average High and Low Temperature and Pulse Rate',
        align: 'left',
      },
      grid: {
        borderColor: '#e7e7e7',
        row: {
          colors: ['#f3f3f3', 'transparent'],
          opacity: 0.5,
        },
      },
      markers: {
        size: 1,
      },
      xaxis: {
        categories: this.vitalSignRound,
        // categories: ['2', '6', '10', '14', '18', '22','2', '6', '10', '14', '18', '22'],

        // title: {
        //   text: 'วัน/รอบ',
        // },
        position: 'top',
        tickPlacement: 'between',
        group: {
          style: {
            fontSize: '10px',
            fontWeight: 700
          },
          // groups: [
          //   { title: '2019', cols: 6 },
          //   { title: '2020', cols: 6 }
          // ],
          groups: this.vitalSignRoundGroup,

        },
        labels: {
          show: true,
          rotate: -45,
          rotateAlways: false,
          hideOverlappingLabels: true,
          showDuplicates: false,
          trim: false,
          minHeight: undefined,
          maxHeight: 120,
          style: {
            colors: [],
            fontSize: '12px',
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontWeight: 400,
            cssClass: 'apexcharts-xaxis-label',
          },
          offsetX: 0,
          offsetY: 0,
          format: undefined,
          formatter: undefined,
          datetimeUTC: true,
          datetimeFormatter: {
            year: 'yyyy',
            month: "MMM 'yy",
            day: 'dd MMM',
            hour: 'HH:mm',
          },
        },

      },
      annotations: {
        xaxis: [{
          x: 7,
          borderColor: '#999',
          yAxisIndex: 0,
          label: {
            show: true,
            text: 'Annotation',
            style: {
              color: "#fff",
              background: '#775DD0'
            }
          }
        }]
      },

      yaxis: [
        {
          title: {
            text: 'Temperature',
          },
          annotations: {
            yaxis: [
              {
                y: 30,
                borderColor: "#00E396",
                label: {
                  borderColor: "#00E396",
                  style: {
                    color: "#fff",
                    background: "#00E396"
                  },
                  text: "Y Axis Annotation"
                }
              }
            ],
          },
          min: 30,
          max: 45,
          tickAmount: 6,

        },
        {
          title: {
            text: 'Pulse Rate',
          },
          min: 70,
          max: 140,
          tickAmount: 6,

        },
      ],
      legend: {
        position: 'top',
        horizontalAlign: 'right',
        floating: true,
        offsetY: -25,
        offsetX: -5,
      },
    };
  }
  async ngOnInit() {
    // console.log(this.parentData);

    // this.sessionPatientInfo = sessionStorage.getItem('itemPatientInfo');
    //itemPatientInfo = an

    // this.itemPatientInfo = JSON.parse(this.sessionPatientInfo);
    this.anFromParent = this.parentData.an;

    //console.log('itemPatientInfo : ', this.itemPatientInfo);
    //ken writing
    await this.listActivity();
    await this.listEvaluate();
    //Patient info
    await this.getNursePatient();
    await this.getDoctor();
    await this.prepareDataChart(this.startdate);
  }


  async getNursenote(id: any) {
    try {
      const respone = await this.vitalSignService.getNurseNote(id);
      const responseData: any = respone.data;
      let data: any = responseData;
      // console.log('Nurse note:', data);
      this.NurseNote = data.data;
    } catch (error: any) {
      // console.log(error);
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    }
  }
  async listActivity() {
    try {
      const respone = await this.vitalSignService.getActivity();
      this.itemActivityInfo = respone.data.data;
      // console.log(respone);
    } catch (error: any) { }
  }

  async listEvaluate() {
    try {
      const respone = await this.vitalSignService.getEvaluate();
      this.itemEvaluate = respone.data.data;
      // console.log(respone);
    } catch (error: any) { }
  }
  //แสดง บันทึกทางการพยาบาล nurse note ake
  showModalOrder(): void {
    this.isVisibleorder = true;
  }
  showModalConsult(): void {
    this.isVisibleconsult = true;
  }

  showModal(): void {
    this.isVisible = true;
  }
  showModalnurse(): void {
    this.isVisiblenurse = true;
  }
  handleOknurse(): void {
    // console.log('Button ok clicked!');
    this.saveNurseNote();
    this.isVisiblenurse = false;
  }

  handleCancelnurse(): void {
    // console.log('Button cancel clicked!');
    this.isVisiblenurse = false;
  }


  //แสดง modal vital sign note
  handleOkVS(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleVS = false;
  }

  handleCancelVS(): void {
    this.isVisibleVS = false;
  }
  handleOkorder(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleorder = false;
  }

  handleCancelorder(): void {

    this.isVisibleorder = false;
  }
  showModalVsAdd() {
    this.rr = '';
    this.bp = '';
    this.dp = '';
    this.bw = '';
    this.bt = '';
    this.pr = '';
    this.painscore = '';
    this.oralfluide = '';
    this.parenterat = '';
    this.urineout = '';
    this.emesis = '';
    this.drainage = '';
    this.stools = '';
    this.aspiration = '';
    this.urine = '';
    this.medications = '';
    this.isVisibleVitalSign = true;

  }
  showModalVSEdit(): void {
    this.isVisibleVSEdit = true;
    this.getNurseVitalSignLast(this.admit_id);
  }
  //แสดง modal vital sign note
  handleOkVSEdit(): void {
    // console.log('Button ok clicked!');
    this.saveNurseVitalsign();
    this.isVisibleVSEdit = false;
  }

  handleCancelVSEdit(): void {
    // console.log('Button cancel clicked!');
    this.isVisibleVSEdit = false;
  }

  async getActivity(i: any, e: any) {
    // console.log(i, ' : ', e);

    if (e.srcElement.checked) {
      this.activity_checked.push(i);
    } else {
      this.removeItemArray(this.activity_checked, i);
    }
    // console.log(this.activity_checked);
  }

  async getEvaluate(i: any, e: any) {
    // console.log(i, ' : ', e);

    if (e.srcElement.checked) {
      this.evaluate_checked.push(i);
    } else {
      this.removeItemArray(this.evaluate_checked, i);
    }
    // console.log(this.evaluate_checked);
  }

  removeItemArray(array: any, item: any) {
    for (var i in array) {
      if (array[i] == item) {
        array.splice(i, 1);
        break;
      }
    }
  }

  async saveNurseNote() {
    let date: Date = new Date();
    // console.log(date);

    let data: any = {
      nurse_note_date: '2023-08-06',
      nurse_note_time: '12:22:00',
      problem_list: [this.problem_list],
      activity: this.activity_checked,
      evaluate: this.evaluate_checked,
      admit_id: this.admit_id,
      create_date: '2023-08-06',
      create_by: sessionStorage.getItem('userID'),
      modify_date: '',
      modify_by: sessionStorage.getItem('userID'),
      is_active: true,
      item_used: [{}],
    };
    // console.log(data);
    try {
      if (this.activity_checked.length || this.evaluate_checked.length) {
        const respone = await this.vitalSignService.saveNurseNote(data);
        // console.log(respone);
      }
    } catch (error: any) { }
  }
  async saveNurseVitalsign() {
    console.log('saveNurseVitalsign');
    const dateTime = DateTime.now();
    const formattedTime = dateTime.toFormat('HH:mm:ss');
    const formattedDate = dateTime.toFormat('yyyy-MM-dd');
    let isValidated: boolean = true;
    console.log('Vigtalsign วันที่: ' + formattedDate);
    let roundtime: any;

    if(this.round_time){
      roundtime=this.round_time;
    }else{
      roundtime=this.selectedRoundNurse.name;
    }
    
    let data: any = {
      admit_id: this.admit_id,
      vital_sign_date: formattedDate,
      vital_sign_time: formattedTime,

      round_time: roundtime,

      respiratory_rate: this.rr,
      systolic_blood_pressure: this.sp,
      diatolic_blood_pressure: this.dp,
      body_weight: this.bw,
      body_temperature: this.bt,
      pulse_rate: this.pr,
      pain_score: this.painscore,

      intake_oral_fluid: this.oralfluide,
      intake_penterate: this.parenterat,

      outtake_urine: this.urineout,
      outtake_emesis: this.emesis,
      outtake_drainage: this.drainage,
      outtake_aspiration: this.aspiration,

      stools: this.stools,
      urine: this.urine,
      intake_medicine: this.medications,

      outtake_lochia: '99',
      oxygen_sat: '99',
      create_at: '',
      create_by: sessionStorage.getItem('user_id'),
      modify_at: '',
      modify_by: sessionStorage.getItem('user_id'),

    };
    console.log("Vigtalsign data: ");
    console.log(data);
    let error_message: string = '';
    if (roundtime =='') {
      error_message += 'ยังไม่ได้บันทึกรอบที่วัด<br>';
      isValidated = false;
    console.log("x1");
    }
    if (this.bt == '' || this.bt == null) {
      error_message += 'ยังไม่ได้บันทึกอุณหภูมิร่างการ<br>';
      isValidated = false;
    console.log("x2");
    }
    if (this.pr == '' || this.pr == null) {
      error_message += 'ยังไม่ได้บันทึกอัตราการเต้นของหัวใจ<br>';
      isValidated = false;
    console.log("x3");
    }
    if (this.sp == '' || this.sp == null) {
      error_message += 'ยังไม่ได้บันทึกความดันโลหิด Systolic<br>';
      isValidated = false;
    console.log("x4");
    }
    if (this.dp == '' || this.dp == null) {
      error_message += 'ยังไม่ได้บันทึกความดันโลหิด Diastolic';
      isValidated = false;
    console.log("x5");
    }
    console.log("xx");
    console.log(isValidated);
    
    if (isValidated) {
      console.log("fon");
      try {
        const response = await this.vitalSignService.saveNurseVitalSign(data);
        // console.log("ken" + response);
        if (response.status === 200) {
          this.hideSpinner();
          this.messageService.add({
            severity: 'success',
            summary: 'บันทึกสำเร็จ #',
            detail: 'รอสักครู่...',
          });
          this.isSaved = true;
          this.isVisibleVitalSign = false;
          window.location.reload();
        }

      } catch (error) {
        // console.log(error);
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด #',
          detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
        });
        setTimeout(() => {
          this.spinner.hide();
        }, 1000);
      }
    } else {

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...',
      });
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

      this.showModalVS();
    }

  }
  showModalVS(): void {
    this.isVisibleVitalSign = true;
  }
  async navigateNurse() {
    // console.log('บันทึกสำเร็จ');
    this.router.navigate(['/nurse/patient-list/nurse-note']);
  }
  // getadmit_id: any;
  async getNursePatient() {
    try {
      const response = await this.vitalSignService.getNursePatient(this.anFromParent);
      // console.log(response);
      // this.getadmit_id = response.data.data[0].admit_id;
      this.address = response.data.data[0].address;
      this.admit_id = response.data.data[0].admit_id;
      this.age = response.data.data[0].age;
      this.an = response.data.data[0].an;
      this.blood_group = response.data.data[0].blood_group;
      this.cid = response.data.data[0].cid;
      this.citizenship = response.data.data[0].citizenship;
      this.create_by = response.data.data[0].create_by;
      this.create_date = response.data.data[0].create_date;
      this.dob = response.data.data[0].dob;
      this.fname = response.data.data[0].fname;
      this.gender = response.data.data[0].gender;
      this.hn = response.data.data[0].hn;
      this.id = response.data.data[0].id;
      this.inscl = response.data.data[0].inscl;
      this.inscl_name = response.data.data[0].inscl_name;
      this.insurance_id = response.data.data[0].insurance_id;
      this.is_active = response.data.data[0].is_active;
      this.is_pregnant = response.data.data[0].is_pregnant;
      this.lname = response.data.data[0].lname;
      this.married_status = response.data.data[0].married_status;
      this.modify_by = response.data.data[0].modify_by;
      this.modify_date = response.data.data[0].modify_date;
      this.nationality = response.data.data[0].nationality;
      this.occupation = response.data.data[0].occupation;
      this.phone = response.data.data[0].phone;
      this.reference_person_address =
        response.data.data[0].reference_person_address;
      this.reference_person_name = response.data.data[0].reference_person_name;
      this.reference_person_phone =
        response.data.data[0].reference_person_phone;
      this.reference_person_relationship =
        response.data.data[0].reference_person_relationship;
      this.religion = response.data.data[0].religion;
      this.title = response.data.data[0].title;

      //ken ดึงข้อมูล เตียงมาแสดง
      this.getPatientdataadmit(this.admit_id);

      //ดึงข้อมูลมาใส่ตัวแปล

      //tab Info
      await this.getNurseInfo();
      //tab Note
      await this.getNursenote(this.admit_id);
      //tab V/S
      await this.getNurseVitalSign(this.admit_id);
      //tab Doctor Order
      await this.getOrder(this.admit_id);
      // console.log('NuPatient',response);
    } catch (error) {
      // console.log(error);
    }
  }
  async getPatientdataadmit(id: any) {
    try {
      const response = await this.vitalSignService.getDatabedanddoc(id);
      for (let i of this.doctors) {
        // console.log("loop docrot iiii",i);
        if (i.user_id == response.data.data[0].doctor_id) {
          // console.log("loop docrot",i);
          this.doctorname = i.title + i.fname + ' ' + i.lname;
          // console.log("ชื่อหมด:",this.doctorname);
        } else {
          // console.log('หาหมอไม่เจอ');
        }
      }

      this.topbed = response.data.data[0].bed_number;
      // console.log('getDatabedanddoc:',response);
    } catch (error) { }
  }
  //หาหมอ
  async getDoctor() {
    // console.log('getDoctor');
    try {
      const response: AxiosResponse = await this.vitalSignService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.doctors = data;
      // console.log('getDoctor : ', this.doctors);
    } catch (error: any) {
      // console.log(error);
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    }
  }

  async getNurseInfo() {
    try {
      const response = await this.vitalSignService.getNurseinfo(this.admit_id);
      // console.log('admit_id:', response);
      this.body_height = response.data.data[0].body_height;
      this.body_temperature = response.data.data[0].body_temperature;
      this.body_weight = response.data.data[0].body_weight;
      this.chief_complaint = response.data.data[0].chief_complaint;
      this.diatolic_blood_pressure =
        response.data.data[0].diatolic_blood_pressure;
      this.eye_score = response.data.data[0].eye_score;
      this.movement_score = response.data.data[0].movement_score;
      this.oxygen_sat = response.data.data[0].oxygen_sat;
      this.past_history = response.data.data[0].past_history;
      this.physical_exam = response.data.data[0].physical_exam;
      this.present_illness = response.data.data[0].present_illness;
      this.pulse_rate = response.data.data[0].pulse_rate;
      this.respiratory_rate = response.data.data[0].respiratory_rate;
      this.systolic_blood_pressure =
        response.data.data[0].systolic_blood_pressure;
      this.verbal_score = response.data.data[0].verbal_score;
      this.waist = response.data.data[0].waist;
    } catch (error) { }
  }
  //สร้างข้อมูล
  async prepareDataChart(date_start: Date) {
    let x: any = [];
    let temperature: any = [];
    let pulse: any = [];
    let dateweek: any = [];
    let vs = this.NurseVitalSign[0].data_vital_sign;
    // for (let v of vs) {
    //   let check: any = v.vital_sign_date.substring(0, 10) + 'R' + v.round_time;
    //   v.checked = check;
    // }
    // console.log('vs', vs);


    // for (let i = 0; i < 7; i++) {

    //   let record_date = new Date(date_start);
    //   record_date.setDate(record_date.getDate() + i);
    //   let next_day = record_date;
    //   next_day.setDate(next_day.getDate() + 1);
    //   let data: any;
    //   let dateloopweek: any;
    //   for (let j = 0; j < 6; j++) {
    //     let r_date: string = record_date.toISOString().substring(0, 10);
    //     let order_round = (((j + 1) * 4) - 2);
    //     data = r_date + 'R' + order_round;
    //     dateloopweek = r_date;
    //     x.push(data);
    //     let _vs: any = vs.find((v: any) => v.checked == data);
    //     if (_vs == undefined) {
    //       temperature.push(0);
    //       pulse.push(0);
    //     } else {
    //       temperature.push(_vs.body_temperature)
    //       pulse.push(_vs.pulse_rate)
    //     }
    //   }
    //   this.nextdate = next_day;
    //   dateweek.push(dateloopweek);
    //   this.datetop = dateweek;



    // }
 


    const dates = vs.map((item: { create_at: string }) => new Date(item.create_at).toISOString().split('T')[0]);
    const uniqueDates = [...new Set(dates)];
    const sortedDates = uniqueDates.sort(); // Sort the unique dates in ascending order
    const result = sortedDates.map(date => ({ title: date, cols: 6  }));

    console.log(result);
    this.vitalSignRoundGroup = result;

    vs.forEach((_vs: any, index: number) => {
      // console.log(index, _vs);
      this.temperature.push(_vs.body_temperature);

      this.pulse.push(_vs.pulse_rate);
      this.vitalSignRound.push(_vs.round_time);
      console.log(this.temperature); console.log(this.pulse); console.log(this.vitalSignRound);



    });
    this.apexChartLine();

    // this.temperature = temperature;
    // this.pulse = pulse;
    this.dataxnamex = x;
    console.log("temperature", this.temperature);
    console.log("pulse", this.pulse);
    console.log("vitalSignRound", this.vitalSignRound);
    // console.log("datax3", x);
    // console.log("dateweek", dateweek);
    const repeatTimes = 7;
    this.roundnurse = Array(repeatTimes).fill(this.roundnurse1).flat();
    // console.log("roundnurse", this.roundnurse);


  }
  async getNurseVitalSignLast(id: any) {
    try {
      const respone = await this.vitalSignService.getNursevital_sign(id);
      const responseData: any = respone.data;
      let data: any = responseData;
      console.log('Nurse vital sign:', data);
      let ds: { id: string; create_at: string }[] = data.data_admit[0].data_vital_sign
      let sort = ds.sort((a, b) => new Date(b.create_at).getTime() - new Date(a.create_at).getTime());
      const lastRecord = sort[0];

      this.NurseVitalSignLast = lastRecord;
      console.log(lastRecord);
      console.log(this.NurseVitalSignLast);
      this.round_time = this.NurseVitalSignLast.round_time
      this.rr = this.NurseVitalSignLast.respiratory_rate;
      this.sp = this.NurseVitalSignLast.systolic_blood_pressure;
      this.dp = this.NurseVitalSignLast.diatolic_blood_pressure;
      this.bw = this.NurseVitalSignLast.body_weight;
      this.bt = this.NurseVitalSignLast.body_temperature;
      this.pr = this.NurseVitalSignLast.pulse_rate;
      this.painscore = this.NurseVitalSignLast.pain_score;
      this.oralfluide = this.NurseVitalSignLast.intake_oral_fluid;
      this.parenterat = this.NurseVitalSignLast.intake_penterate;
      this.urineout = this.NurseVitalSignLast.outtake_urine;
      this.emesis = this.NurseVitalSignLast.outtake_emesis;
      this.drainage = this.NurseVitalSignLast.outtake_drainage;
      this.stools = this.NurseVitalSignLast.stools;
      this.aspiration = this.NurseVitalSignLast.outtake_aspiration;
      this.urine = this.NurseVitalSignLast.urine;
      this.medications = this.NurseVitalSignLast.intake_medicine;

console.log(this.round_time);


    } catch (error) { }
  }
  async getNurseVitalSign(id: any) {
    try {
      const respone = await this.vitalSignService.getNursevital_sign(id);
      const responseData: any = respone.data;
      let data: any = responseData;
      // console.log('Nurse vital sign:', data);
      this.NurseVitalSign = data.data_admit;
      this.startdate = data.data_admit[0].admit_date;
      // console.log('v/s:', this.NurseVitalSign[0].data_vital_sign[0].body_temperature);
    } catch (error) { }
  }

  async getOrder(id: any) {
    try {
      const respone = await this.vitalSignService.getorder(id);
      const responseData: any = respone.data;
      let data: any = responseData.getAdmitID;
      // console.log('doctor order:', data);
      this.doctorOrder = data;
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
    }
  }


  //ken
  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
  backPage() {
    this.location.back();
  }


}

