import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Location} from '@angular/common';
import { AxiosResponse } from 'axios';
import * as _ from 'lodash';
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';
import { MenuItem, SelectItem } from 'primeng/api';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import { LookupService } from '../../../shared/lookup-service';
import { NurseNoteService } from './nurse-note.service';

interface AutoCompleteCompleteEvent {
  originalEvent: Event;
  query: string;
}
interface OptionItemsDoctors {
  id: string;
  fname: string;
  default_usage: string;
  item_type_id: string;
}
interface OptionItemsWards {
  id: string;
  name: string;  
}
interface OptionItemsBeds {
  bed_id: string;
  name: string;  
}


@Component({
  selector: 'app-nurse-note',
  templateUrl: './nurse-note.component.html',
  styleUrls: ['./nurse-note.component.scss'],
  providers: [MessageService, ConfirmationService],

})
export class NurseNoteComponent implements OnInit {
  query: any = '';
  dataSet: any[] = [];
  dataSetReview: any[] = [];
  dataSetAdmit: any[] = [];
  dataSetWaitingInfo: any[] = [];
  dataSetTreatement: any[] = [];
  dataSetPatientAllergy: any[] = [];
  loading = false;

 
  isChecked = [];
  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  wardId: any;
  doctorId: any;

  is_select_problem_list:boolean=false;
  is_select_Evaluate:boolean=false;
  is_select_Activity:boolean=false;
  is_requirefield:boolean=false;

  queryParamsData: any;
  doctorBy: any;
  preDiag: any;
  chieft_complaint: any;
  panelsWard: any[] = [];
  panelsBed: any[] = [];
  bedId: any;
  isLoading: boolean = true;
  isVisible = false;
  userId: any;
  departmentId: any;
  isSaved = false;
  btnSaveIsVisible = true;
  selectedValue: any = null;
  speedDialitems: MenuItem[] = [];
  selectedDrop: SelectItem = { value: '' };
  visibleNursenotesidebar: boolean = false;
  itemActivityInfo: any;
  itemEvaluate: any;

 
  cities:any = [
    { label: 'New York', value: { id: 1, name: 'New York', code: 'NY' } },
    { label: 'Rome', value: { id: 2, name: 'Rome', code: 'RM' } },
    { label: 'London', value: { id: 3, name: 'London', code: 'LDN' } },
    { label: 'Istanbul', value: { id: 4, name: 'Istanbul', code: 'IST' } },
    { label: 'Paris', value: { id: 5, name: 'Paris', code: 'PRS' } }
];
doctors:any = [
  { label: 'Istanbul', value: 1 },
    { label: 'Paris', value: 2 }
];
wards:any = [];
beds:any = [];

selectedItemsDoctor: any;
filteredItemsDoctors: any | undefined;
optionsItemsDoctors: OptionItemsDoctors[] = [];

selectedItemsWards: any;
filteredItemsWards: any | undefined;
optionsItemsWards: OptionItemsWards[] = [];

selectedItemsBeds: any;
filteredItemsBeds: any | undefined;
optionsItemsBeds: OptionItemsBeds[] = [];

dataLoaded: boolean = false;
parentData:any;
txtarea_problem_list :any="";
txtarea_activity: any;
txtarea_evaluate:any;
activity_checked: any = [];
evaluate_checked: any = [];
anFromParent: any;
admit_id: any;
optionActivitySelected :any = [];
optionEvaluateSelected : any = [];
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,

    private nurseNoteService: NurseNoteService,

    private lookupService: LookupService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private spinner: NgxSpinnerService,
    private location: Location,

  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    // console.log('queryParamsData : ', this.queryParamsData);


  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }
  filterItemsDoctors(event: AutoCompleteCompleteEvent) {
    let filtered: any[] = [];
    let query = event.query.toLowerCase();

    for (let i = 0; i < this.optionsItemsDoctors.length; i++) {
        let item = this.optionsItemsDoctors[i];
        // console.log(item);
        if (item.fname.toLowerCase().indexOf(query) !== -1) {
            filtered.push(item);
        }
    }
    this.filteredItemsDoctors = filtered;
}
filterItemsWards(event: AutoCompleteCompleteEvent) {
  let filtered: any[] = [];
  let query = event.query.toLowerCase();

  for (let i = 0; i < this.optionsItemsWards.length; i++) {
      let item = this.optionsItemsWards[i];
      // console.log(item);
      if (item.name.toLowerCase().indexOf(query) !== -1) {
          filtered.push(item);
      }
  }
  this.filteredItemsWards = filtered;
}
filterItemsBeds(event: AutoCompleteCompleteEvent) {
  let filtered: any[] = [];
  let query = event.query.toLowerCase();

  for (let i = 0; i < this.optionsItemsBeds.length; i++) {
      let item = this.optionsItemsBeds[i];
      // console.log(item);
      if (item.name.toLowerCase().indexOf(query) !== -1) {
          filtered.push(item);
      }
  }
  this.filteredItemsBeds = filtered;
}
onSelectedWard(e: any) {
  // console.log(e);
  let data = e;
  this.getBed(data.id);
}
onSelectedBed(e: any) {
  // console.log(e);
  let data = e;
  // console.log(this.selectedItemsBeds);
}
  buttonSpeedDial(){
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'left'
        },
      },
	  {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          // this.navigatePatientInfo()
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          // this.navigateDoctorOrder()
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          // this.navigateEkg()
        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          // this.navigateConsult()
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'left'
        },
      },
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          // this.navigateLab()
        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'left'
        },
      }
    ];
  }


  async ngOnInit() {
    this.parentData = this.queryParamsData;
    this.anFromParent = this.parentData.an;
    // console.log( this.parentData);
    this.dataLoaded =true;
    await this.getNursePatient();
    await this.listActivity();
    await this.listEvaluate();

    // if(this.parentData.length > 0){
    //   this.dataLoaded =true;
    //   console.log('param reciveed');

    // }else{
    //   console.log('no param recive');
    // }

    this.buttonSpeedDial();
    this.getWaitingInfo();

    this.getReview();
    this.getAdmit();
    this.getWard();
    this.getDoctor();
    this.getTreatement();
    this.getPatientAllergy();
    this.assignCollapseWard();

  }
//ken
async getNursePatient(){
  try {
    console.log( "this.anFromParent",this.anFromParent);
    const response = await this.nurseNoteService.getNursePatient(this.anFromParent);
    console.log("getNursePatient NT: ",response);
    this.admit_id = response.data.data[0].admit_id;

  } catch (error) {
    console.log(error);
  }
}
async SaveNursenote(): Promise<void> {
  await this.saveNurseNotedata();
}
async saveNurseNotedata() {
  var date_now = DateTime.now();
  const datenow = date_now.toFormat('yyyy-MM-dd');
  const timenow = date_now.toFormat('HH:mm:ss');
  this.optionActivitySelected.push(this.txtarea_activity);
  this.optionEvaluateSelected.push(this.txtarea_evaluate);
  // console.log(date);
  let data: any = {
    nurse_note_date: datenow,
    nurse_note_time: timenow,
    problem_list: [this.txtarea_problem_list],
    activity: this.optionActivitySelected,
    evaluate: this.optionEvaluateSelected,
    admit_id: this.admit_id,
    create_date: datenow,
    create_by: sessionStorage.getItem('user_id'),
    modify_date: datenow,
    modify_by: sessionStorage.getItem('user_id'),
    is_active: true,
    item_used: [{}],
  };
   console.log("ข้อมูลก่อนบันทึก : ",data,this.optionActivitySelected);
  try {
       const respone = await this.nurseNoteService.saveNurseNote(data);
       if(respone.status === 200){
        // alert("บันทึกสำเร็จ");
        this.visibleNursenotesidebar = false;
        this.messageService.add({
          severity: 'success',
          summary: 'บันทึกสำเร็จ #',
          detail: 'บันทึกทางการพยาบาลสำเร็จกรุณาตรวจสอบข้อมูลหลังบันทึก...',
        });
        window.location.reload();
       }else{
        // alert(respone.status);
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด #',
          detail: 'กรุณาติดต่อ admin ...',
        });
       }
      //  console.log("ตรวจสอบการบันทึก: ",respone);
   
  } catch (error: any) { }
}
disable_botton_save(){
  if(this.is_select_Activity == true && this.is_select_Evaluate ==true && this.is_select_problem_list==true){
    this.is_requirefield=true;
  }
}
optionActivityClick(e: any) {
  if (e.checked.length > 0) {
    this.is_select_Activity=true;
    this.disable_botton_save();
      this.optionActivitySelected.push(e.checked[0]);
  } else {
      this.optionActivitySelected.splice(
          this.optionActivitySelected.indexOf(e.checked[0]),1
      );
  }
}

keypress_problem_list(e:any){
  this.is_select_problem_list=true;
  this.disable_botton_save();
}
optionEvaluateClick(e: any) {

  if (e.checked.length > 0) {
    this.is_select_Evaluate=true;
    this.disable_botton_save();
      this.optionEvaluateSelected.push(e.checked[0]);
  } else {
      this.optionEvaluateSelected.splice(
          this.optionEvaluateSelected.indexOf(e.checked[0]),1
      );
  }
}
async openSlidebarnursenote()  {
  this.visibleNursenotesidebar = true;

  if(this.is_requirefield == true){
    // const respone = await this.nurseNoteService.getActivity();
    // this.itemActivityInfo = respone.data.data;
    this.is_requirefield=false;
    this.isChecked = [];
    this.txtarea_problem_list="";
    this.txtarea_evaluate="";
    this.txtarea_activity="";
    // await this.listActivity();
    //   this.listEvaluate();
    // console.log( this.itemActivityInfo );

    // this.dataSet =this.itemActivityInfo.filter(
    //    (x:any).forEach(x) =>( x.isChecked = false));

    //   this.itemEvaluate= this.itemEvaluate.filter(
    //     (x:any) => x.isChecked = false);
  } 
  


}
async listActivity() {
  try {
    const respone = await this.nurseNoteService.getActivity();
    this.itemActivityInfo = respone.data.data;
    // console.log("listActivity555: ",respone);
  } catch (error: any) {}
}
async listEvaluate() {
  try {
    const respone = await this.nurseNoteService.getEvaluate();
    this.itemEvaluate = respone.data.data;
    // console.log("listEvaluate5555: ",respone);
  } catch (error: any) {}
}
  async getReview() {
    this.spinner.show();
    try {
      const response = await this.nurseNoteService.getReview(this.queryParamsData.an)
      const data = await response.data;
      this.dataSetReview = await data;
      // console.log('getReview : ', this.dataSetReview);
      this.isLoading = false;
      this.hideSpinner();
      // this.notificationService.notificationSuccess('คำชี้แจ้ง', 'กระบวนการทำงานสำเร็จ..', 'top');
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });

    }
  }

  async getTreatement() {
    try {
      const response = await this.nurseNoteService.getTreatement(this.queryParamsData.an)
      const data = response.data;
      this.dataSetTreatement = await data;
      // console.log('getTreatement : ', this.dataSetTreatement);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
    }
  }

  async getPatientAllergy() {
    try {
      const response = await this.nurseNoteService.getPatientAllergy(this.queryParamsData.an)
      const data = response.data;
      this.dataSetPatientAllergy = await data;
      // console.log('getPatientAllergy : ', this.dataSetPatientAllergy);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
    }
  }

  async getAdmit() {
    // console.log('getAdmit');
    // console.log(this.queryParamsData.an);
    try {
      const response = await this.nurseNoteService.getWaitingAdmit(this.queryParamsData.an);
      const data = await response.data;
      // console.log(data);
      this.dataSetAdmit = await data;
      if (!this.dataSetAdmit[0]) {
        // this.notificationService.notificationError('คำชี้แจ้ง', 'ไม่พบข้อมูล Admit!', 'top');
        this.btnSaveIsVisible = false;
      }
      // console.log('getAdmit : ', this.dataSetAdmit);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      // console.log(error);

      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    }
  }
  async getWaitingInfo() {
    // console.log('getWaitingInfo');
    try {
      const response = await this.nurseNoteService.getWaitingInfo(this.queryParamsData.an)
      // console.log(this.queryParamsData.an);
      const data = await response.data;
      this.dataSetWaitingInfo = await data;
      this.preDiag = this.dataSetWaitingInfo[0].pre_diag;
      this.doctorBy = this.dataSetWaitingInfo[0].admit_by
      if (!this.doctorBy || !this.preDiag) {
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด #',
          detail: 'กรุณาติดต่อผู้ดูแลระบบ...',
        });
        this.btnSaveIsVisible = false;
      }
      // console.log('getWaitingInfo : ', this.dataSetWaitingInfo);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {

      // console.log(error);
      // this.hideSpinner();
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);

    }
  }
  async getWard() {
    try {
      const response: AxiosResponse = await this.lookupService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      // console.log('Ward : ', this.wards);
      this.optionsItemsWards = responseData.data;
      // console.log(this.optionsItemsWards);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });

    }
  }

  async getDoctor() {
    try {
      const response: AxiosResponse = await this.lookupService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      
      this.optionsItemsDoctors = responseData.data;
      // console.log(this.optionsItemsDoctors);

      this.doctors = data;
      if (!_.isEmpty(data)) {
        // this.doctorId = .id;
      }
      // console.log('Doctor : ', this.doctors);
    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });

    }
  }

  assignCollapseWard() {
    this.panelsWard = [
      {
        active: true,
        name: 'เลือกตึก',
        disabled: false
      },
    ];
  }

  assignCollapseBed() {
    this.panelsBed = [
      {
        active: true,
        name: 'เลือกเตียง',
        disabled: false
      },
    ];
  }

  setWard(data: any) {
    // console.log(data);

    this.wardId = data.id;
    let wardName = data.name;
    this.departmentId = data.department_id;
    this.panelsWard = [
      {
        active: false,
        name: 'เลือกตึก : ' + wardName,
        disabled: false
      },
    ];
    // console.log('wardId_now : ', this.wardId);
    this.bedId = '';
    this.getBed(this.wardId);
  }

  async getBed(wardId: any) {
    try {
      // console.log('wardsId : ', wardId);
      const response: AxiosResponse = await this.lookupService.getBed(wardId);
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.beds = data;
     
      // console.log('Beds : ', this.beds);
      this.optionsItemsBeds = responseData.data;

    } catch (error: any) {
      // console.log(error);
      this.hideSpinner();

      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });


    }
  }

  setBed(data: any) {
    // console.log('dataBeds : ', data);

    this.bedId = data.bed_id;
    let bedName = data.name;
    this.panelsBed = [
      {
        active: false,
        name: 'เลือกเตียง : ' + bedName,
        disabled: false
      },
    ];
    // console.log('bedId : ', this.bedId);
  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

 
  async saveRegister() {
    if(!this.selectedItemsWards.id && !this.selectedItemsBeds.bed_id){
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด !',
        detail: 'กรูณาตรวจสอบกรอกข้อมูลให้ครบ',
      });
    }else{
      // console.log(this.wardId);
      this.isSaved = true;
      this.spinner.show();
      let info: any = {
        "patient": this.dataSetWaitingInfo,
        "review": this.dataSetReview,
        "treatment": this.dataSetTreatement,
        "admit": this.dataSetAdmit,
        "bed": [
          {
            "bed_id": this.selectedItemsBeds.bed_id,
            "status": "Used"
          }
        ],
        "ward": [
          {
            "ward_id": this.selectedItemsWards.id,
          }
        ],
        "doctor": [
          { "user_id": this.selectedItemsDoctor.id }
        ],
        "department": [
          { "department_id": this.selectedItemsWards.department_id }
        ],
        "patient_allergy": this.dataSetPatientAllergy
      }
      // console.log(info);
      try {
        const response = await this.nurseNoteService.saveRegister(info);
        // console.log(response);
        if (response.status === 200) {
          this.hideSpinner();
          this.messageService.add({
            severity: 'success',
            summary: 'บันทึกสำเร็จ #',
            detail: 'รอสักครู่...',
          });
          this.isSaved = true;
          setTimeout(() => {
            this.navigateWaiting();
          }, 2000);
  
        }
  
      } catch (error) {
        // console.log(error);
        this.hideSpinner();
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด !',
          detail: 'กรูณาตรวจสอบ',
        });
      }
    }
  }
  

  async navigateWaiting() {
    // const confirm = await this.showConfirmModal('คำชี้แจ้ง', 'คุณยังไม่ได้บันทึกข้อมูล คุณแน่ใจว่าจะยกเลิกกระบวนการนี้ !');

    // if (confirm) {
    //   this.isSaved = true;
    this.router.navigate(['/waiting-admit']);
    //   }
  }
  backPage() {
    this.location.back();
  }



}
