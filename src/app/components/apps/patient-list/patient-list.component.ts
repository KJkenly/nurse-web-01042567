import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AxiosResponse } from 'axios';
import * as _ from 'lodash';
import { DateTime } from 'luxon';
import { Table } from 'primeng/table';
import { MenuItem } from 'primeng/api';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import { LookupService } from '../../../shared/lookup-service';
import { Calendar } from 'primeng/calendar';
import { PatientListService } from './patient-list.service';
import { PrimeNGConfig } from 'primeng/api';
interface AutoCompleteCompleteEvent {
    originalEvent: Event;
    query: string;
  }
interface OptionItemsWards {
    id: string;
    name: string;  
  }
  interface OptionItemsBeds {
    bed_id: string;
    name: string;  
  }


@Component({
    selector: 'app-patient-list',
    templateUrl: './patient-list.component.html',
    styleUrls: ['./patient-list.component.scss'],
    providers: [MessageService, ConfirmationService],
})
export class PatientListComponent implements OnInit {
    admitId: any;
    doctorID: any;
    dataWard: any;
    wardName: any;
    wardNameFirst:any;
    wards: any = [];
    beds:any = [];
    query: any = '';
    dataSet: any[] = [];
    loading = false;
    doctors: any = [];
    th: any;

    is_select_bed: boolean=false;    
    selectedBed: any;
   // selectedWard:any;

    // selectedDate: any;
    selectedDate: Date | undefined;
    total = 0;
    pageSize = 20;
    pageIndex = 1;
    offset = 0;
    user_login_name: any;
    isVisible = false;
    isOkLoading = false;
    inputValue?: string;
    modalTitle: string = 'ระบุแพทย์เจ้าของไข้';
    selectedValue: any = null;
    queryParamsData: any;
    selectedWard: any = [];
    date2: Date | undefined;
    activeItem: any;
    nodes: any =
        [
            {
                key: '0',
                label: 'Documents',
                data: 'Documents Folder',
                icon: 'pi pi-fw pi-inbox',
            },
            {
                key: '0',
                label: 'Documents',
                data: 'Documents Folder',
                icon: 'pi pi-fw pi-inbox',
            }

        ];
    visibleChangDoctor: boolean = false;
    visibleChangWard: boolean = false;
    items: MenuItem[] = [
        {
            label: '',
            icon: 'pi pi-fw pi-file',
            items: [
                {
                    label: 'ย้ายตึก/เตียง',
                    icon: 'pi pi-fw pi-file-import',
                    // routerLink: '/change-ward'
                    command: (e) => {
                        console.log(this.activeItem);
                        this.visibleChangWard = true;
                        this.is_select_bed = false;
                        this.selectedItemsWards="";
                        this.selectedItemsBeds="";
                    }
                },
                {
                    label: 'ระบุแพทย์',
                    icon: 'pi pi-fw pi-user-plus',
                    command: (e) => {
                        console.log(this.activeItem);
                        this.visibleChangDoctor = true;
                    }

                },
                {
                    separator: true
                },
                {
                    label: 'ลบรายการ',
                    icon: 'pi pi-fw pi-trash',
                    command: (e) => {
                        console.log(this.activeItem);
                        this.deleteAdmit(this.activeItem);
                    }

                }
            ]
        },

    ];
selectedItemsWards: any;
filteredItemsWards: any | undefined;
optionsItemsWards: OptionItemsWards[] = [];
selectedItemsBeds: any;
filteredItemsBeds: any | undefined;
optionsItemsBeds: OptionItemsBeds[] = [];
message = 'Calendar set in french, everything is fine';
    @ViewChild('calendar')
    calendar!: Calendar;

en = {
    firstDayOfWeek: 0,
    dayNames: ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์'],
    dayNamesShort: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
    monthNames: ['มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฎาคม',
    'สิงหาคม',
    'กันยายน',
    'ตุลาคม',
    'พฤศจิกายน',
    'ธันวาคม'],
    monthNamesShort: ['ม.ค.',
    'ก.พ.',
    'มี.ค.',
    'เม.ย.',
    'พ.ค.',
    'มิ.ย.',
    'ก.ค.',
    'ส.ค.',
    'ก.ย.',
    'ต.ค.',
    'พ.ย.',
    'ธ.ค.'],
    today: 'Today',
    weekHeader: 'Wk'
};

fr = {
    firstDayOfWeek: 1,
    dayNames: ['อาทิตย์','จันทร์','อังคาร','พุธ','พฤหัสบดี','ศุกร์','เสาร์'],
    dayNamesShort: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
    monthNames: ['มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฎาคม',
    'สิงหาคม',
    'กันยายน',
    'ตุลาคม',
    'พฤศจิกายน',
    'ธันวาคม'],
    monthNamesShort: ['ม.ค.',
    'ก.พ.',
    'มี.ค.',
    'เม.ย.',
    'พ.ค.',
    'มิ.ย.',
    'ก.ค.',
    'ส.ค.',
    'ก.ย.',
    'ต.ค.',
    'พ.ย.',
    'ธ.ค.'],
    today: 'วันนี้',
    clear: 'ยกเลิก',
    dateFormat: 'dd/mm/yy',
    weekHeader: 'Wk'
};
    listPatients :any = [];

    constructor(
        public primengConfig: PrimeNGConfig,
        private router: Router,
        private activatedRoute: ActivatedRoute,

        private patientListService: PatientListService,

        private lookupService: LookupService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private spinner: NgxSpinnerService,
        
    ) 
    {

        this.setLangFR();
        this.primengConfig.translationObserver.subscribe(res =>console.log(res));
          
        let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        console.log(jsonObject);
        const localStorageWardId = localStorage.getItem('ward_id');
        if (jsonObject != null || jsonObject != undefined) {
            this.queryParamsData = jsonObject;
        } else {
            this.queryParamsData = localStorageWardId;
        }
        this.dataWard = { id: this.queryParamsData };
        console.log(this.queryParamsData);

        this.wardName = this.activatedRoute.snapshot.queryParamMap.get('wardName');
        this.wardNameFirst = this.wardName;

    }
    setLangFR() {
        this.primengConfig.setTranslation(this.fr);
      }
    
      setLangEN() {
        this.calendar.firstDayOfWeek = 1;
        this.primengConfig.setTranslation(this.en);
        // this.message = "Open the calendar now, monthes are ok, but watch days, they are not translated";
      }
    async ngOnInit() {
        // this.user_login_name  =  this.userProfileService.user_login_name;
        this.user_login_name = sessionStorage.getItem('userLoginName');

        console.log(this.user_login_name);
        await this.getWard();
        await this.getDoctor();
        var offset = new Date().getTimezoneOffset();
        console.log("offset :",offset);
        console.log(Intl.DateTimeFormat().resolvedOptions().timeZone);

    }
    /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
    hideSpinner() {
        setTimeout(() => {
            this.spinner.hide();
        }, 1000);
    }

    async handleOk() {
        this.isOkLoading = true;
        setTimeout(async () => {
            console.log(this.selectedValue);
            let data: any = {
                doctor_id: this.selectedValue,
            };
            const response: AxiosResponse =
                await this.patientListService.changeDoctor(data, this.admitId);
            this.admitId = null;
            this.selectedValue = null;
            this.isVisible = false;
            this.isOkLoading = false;
            this.getWard();
        }, 3000);
    }

    handleCancel(): void {
        this.admitId = null;
        this.selectedValue = null;
        this.isVisible = false;
    }
    onGlobalFilter(table: Table, event: Event) {
        console.log("onGlobalFilter : ",event)
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }
    filterItemsWards(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();
      
        for (let i = 0; i < this.optionsItemsWards.length; i++) {
            let item = this.optionsItemsWards[i];
            // console.log(item);
            if (item.name.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItemsWards = filtered;
      }
      filterItemsBeds(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();
      
        for (let i = 0; i < this.optionsItemsBeds.length; i++) {
            let item = this.optionsItemsBeds[i];
            // console.log(item);
            if (item.name.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItemsBeds = filtered;
      }
      
      onSelectedWard(e: any) {
       // console.log(e);
      let data = e;
        this.getBed(data.id);
        this.selectedWard=data.id;
       console.log("Ward="+this.selectedWard);
       this.is_select_bed=false;
      }
      async getBed(wardId: any) {
        try {
         // console.log('wardsId : ', wardId);
          const response: AxiosResponse = await this.lookupService.getBed(wardId);
          const responseData: any = response.data;
          let  data: any = responseData.data;
        for(let i of data){
            i.bed_no = i.type_name + i.name;
        }

          this.beds = data;
         
          console.log('Beds : ', this.beds);
          this.optionsItemsBeds = responseData.data;
    
        } catch (error: any) {
          console.log(error);
          this.hideSpinner();
    
          this.messageService.add({
            severity: 'error',
            summary: 'เกิดข้อผิดพลาด #',
            detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
          });
    
    
        }
      }
      onSelectedBed(e: any) {
     //   console.log(e);
      //  let data = e;
   
        this.selectedBed =this.selectedItemsBeds;
        console.log(this.selectedBed);
        this.is_select_bed=true;
      }
    displayFormAdd() {
        console.log('displayFormAdd');
    }
    displayFormEdit(data: any) {
        // this.isAdd = false;
        // this.isEdit = true;
        // this.hospitalIdEdit = data.hospital_id;
        // this.hospitalCodeEdit = data.hospital_code;
        // this.hospitalNameEdit = data.hospital_name;
        // this.hospitalActiveEdit = data.is_active;
        // this.displayForm = true;
    }
    async getDoctor() {
        console.log('getDoctor');
        try {
            const response: AxiosResponse =
                await this.lookupService.getDoctor();
            const responseData: any = response.data;
            const data: any = responseData.data;
            this.doctors = data;
            console.log('Doctor : ', this.doctors);
        } catch (error: any) {
            console.log(error);
            this.messageService.add({
                severity: 'dager',
                summary: 'เกิดข้อผิดพลาด #',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    doSearch() {
        this.getAdmitActive();
    }
    logOut() {
        sessionStorage.setItem('token', '');
        return this.router.navigate(['/login']);
    }

    onPageIndexChange(pageIndex: any) {
        this.offset = pageIndex === 1 ? 0 : (pageIndex - 1) * this.pageSize;

        this.getAdmitActive();
    }

    onPageSizeChange(pageSize: any) {
        this.pageSize = pageSize;
        this.pageIndex = 1;

        this.offset = 0;

        this.getAdmitActive();
    }

    refresh() {
        this.query = '';
        this.pageIndex = 1;
        this.offset = 0;
        this.getAdmitActive();
    }

    async onSelectWard(event: any) {
        console.log("event : ",event);

        this.dataWard = event.value;
        if(event.value === null){
            this.wardName = "";
            this.dataSet = [];
        }else{
            this.getAdmitActive();
            this.wardName = event.value.name;
            
        }
    }

    // async getAdmitActive() {
    //     // console.log('getAdmitActive');

    //     this.spinner.show();

    //     try {
    //         const _limit = this.pageSize;
    //         const _offset = this.offset;
    //         const response = await this.patientListService.getAdmitActive(
    //             this.dataWard.id,
    //             this.query,
    //             _limit,
    //             _offset
    //         );
    //         const data: any = response.data;
    //         console.log(data);
    //         this.total = data.total || 1;
    //         this.listPatients = data.data.map((v: any) => {
    //             // สร้างวัตถุ Date จาก v.admit_date
    //         let _date = new Date(v.admit_date);
    //         // แปลงวันที่เป็น ISO 8601 และแยกส่วนของวันที่ออกมา
    //         let _time = _date.getTime() + (7*60*60*1000);
    //         v.search_date = new Date(_time).toISOString().split('T')[0];

    //             const date = v.admit_date
    //                 ? DateTime.fromISO(v.admit_date)
    //                     .setLocale('th')
    //                     .toLocaleString(DateTime.DATE_MED)
    //                 : '';
    //             v.admit_date = date;

    //             const time = v.admit_time
    //                 ? DateTime.fromFormat(v.admit_time, 'HH:mm:ss').toFormat(
    //                     'HH:mm'
    //                 )
    //                 : '';
    //             v.admit_time = time;
    //             // let doctor = this.doctors.find((x:any) => x.user_id === v.doctor_id);
    //          //   console.log('doctor:', doctor.title + doctor.fname + ' ' + doctor.lname);

    //         //    v.doctor_name ="xx"; //doctor.title + doctor.fname + ' ' + doctor.lname;
    //         });
       
    //         this.dataSet = this.listPatients;
    //         console.log('doctor:',   this.listPatients );

    //     } catch (error: any) {
    //         // this.wardName  = this.wardNameFirst;
    //         this.messageService.add({
    //             severity: 'dager',
    //             summary: 'เกิดข้อผิดพลาด #',
    //             detail: 'กรุณาติดต่อผู้ดูแลระบบ5555555...' + error,
    //         });
    //     }
    // }

    async getAdmitActive() {
        // console.log('getAdmitActive');

        this.spinner.show();

        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.patientListService.getAdmitActive(
                this.dataWard.id,
                this.query,
                _limit,
                _offset
            );
            const data: any = response.data;
            console.log(data);

            this.total = data.total || 1;
            this.listPatients = data.data.map((v: any) => {
            // //หาหมอ
            // let doctor = this.doctors.find((x:any) => x.user_id === v.doctor_id);
            // v.drname = doctor.lname;
                // สร้างวัตถุ Date จาก v.admit_date
            let _date = new Date(v.admit_date);
            // แปลงวันที่เป็น ISO 8601 และแยกส่วนของวันที่ออกมา
            let _time = _date.getTime() + (7*60*60*1000);
            v.search_date = new Date(_time).toISOString().split('T')[0];
            // const searchDateConvert = v.search_date
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                        .setLocale('th')
                        .toLocaleString(DateTime.DATE_MED)
                    : '';
                v.admit_date = date;
                const time = v.admit_time
                    ? DateTime.fromFormat(v.admit_time, 'HH:mm:ss').toFormat(
                        'HH:mm'
                    )
                    : '';
                v.admit_time = time;
                let doctor = this.doctors.find((x:any) => x.user_id === v.doctor_id);
               
                if(doctor){
                    v.doctor_name = doctor.fname +' ' + doctor.lname;

                }else{

                   v.doctor_name ="-";
                }
                console.log('v.doctor_id:', v.doctor_id);

    //         //    v.doctor_name ="xx"; //doctor.title + doctor.fname + ' ' + doctor.lname;

                return v;
            });
            this.dataSet = this.listPatients;

        } catch (error: any) {
            // this.wardName  = this.wardNameFirst;
            this.messageService.add({
                severity: 'dager',
                summary: 'เกิดข้อผิดพลาด #',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    async getWard() {
        console.log('getWard');
        try {
            const response: AxiosResponse = await this.lookupService.getWard();
            const responseData: any = response.data;
            const data: any = responseData.data;
            this.wards = data;
            this.optionsItemsWards = data;
            console.log(this.wards);

            if (!_.isEmpty(data)) {
                console.log("data : ",data);
                const selectedWard: any = data[0];
                this.dataWard = selectedWard;
                this.wardName = selectedWard.name;
                this.selectedWard = selectedWard;
            }
            await this.getAdmitActive();
        } catch (error: any) {
            console.log(error);
        }
    }
    // clearSelectedWard() {

    //     console.log("clear : ");
    // }
    async getWardName(wardId: any) {
        const rsWardName = await this.lookupService.getWardName(wardId);
        console.log(rsWardName.data.data[0]);
        return rsWardName.data.data[0];
    }

    changeWard(event: Event) {
     //   let data = event;
       // console.log(data.id);
        // let dk: any = { admit_id: data };
        // let jsonString = JSON.stringify(dk);
        //   console.log("selectedItemsWards=",this.selectedItemsWards.id);
        // console.log("selectedItemsBeds=",this.selectedItemsBeds.bed_id);
         
        // this.router.navigate(['/nurse/patient-list/change-ward'], {
        //     queryParams: { data: jsonString },
        // });

        this.confirmationService.confirm({
            target: event.target as EventTarget,
            message: 'Are you sure that you want to proceed?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            acceptIcon: 'none',
            rejectIcon: 'none',
            rejectButtonStyleClass: 'p-button-text',
            accept: async () => {
                let data: any = {
                    is_active: false,
                };
             //   this.activeItem;
              
                let admitid = this.activeItem.id;
             
                console.log("id=",admitid);
                let json_string ={
                        "bed_id":this.selectedItemsBeds.bed_id,
                        "bed_number":this.selectedItemsBeds.bed_no,
                        "ward_id":this.selectedItemsWards.id
                }
                console.log("json_string=",json_string);
          

                const response: AxiosResponse = await this.patientListService.changeDoctor(admitid, json_string);
                console.log("response.status=",response.status);
                this.visibleChangWard = false;
            if(response.status == 200){
               this.getWard();
             //   this.getDoctor();
                this.isVisible = true;
                this.is_select_bed=false;  
            }
            },
            reject: () => {
                this.messageService.add({
                    severity: 'error',
                    summary: 'Rejected',
                    detail: 'You have rejected',
                    life: 3000,
                });
            },
        });

    }
    async nurseNote(data: any) {
        // console.log(data);
        // let dk: any = { data };
        // console.log('dk : ', dk);

        // let jsonString = JSON.stringify(dk);
        // console.log(jsonString);
        sessionStorage.setItem('itemPatientInfo', JSON.stringify(data));

        console.log('itemPatientInfo data : ', data);

        await this.router.navigate(['/nurse/patient-list/nurse-note']); //, { queryParams: { data: jsonString } });
    }

    async changeDoctor(item: any,event: Event) {
        
        this.confirmationService.confirm({
            target: event.target as EventTarget,
            message: 'Are you sure that you want to proceed?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            acceptIcon: 'none',
            rejectIcon: 'none',
            rejectButtonStyleClass: 'p-button-text',
            accept: async () => {
                let data: any = {
                    is_active: false,
                };
                // const response: AxiosResponse =
                //     await this.patientListService.changeDoctor(data, 'id');
                // this.getWard();
                // // this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted' });
                
                    this.activeItem;
                    let doctor_id = item.user_id;
                    let admitid = this.activeItem.id;
                    console.log("iddoctor : ",doctor_id);
                    console.log("admitid : ",admitid);

                    let json_string ={
                            "doctor_id":doctor_id
                    }
                    console.log(json_string);
                    const response: AxiosResponse = await this.patientListService.changeDoctor(admitid, json_string);
                    console.log("response.status=",response.status);
                    this.visibleChangDoctor = false;
                
                    this.getWard();
                    this.getDoctor();
                    this.isVisible = true;
            },
            reject: () => {
                this.messageService.add({
                    severity: 'error',
                    summary: 'Rejected',
                    detail: 'You have rejected',
                    life: 3000,
                });
            },
        });


    }

    async deleteAdmi7t(event: Event) {
        this.confirmationService.confirm({
            target: event.target as EventTarget,
            message: 'Are you sure that you want to proceed?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            acceptIcon: 'none',
            rejectIcon: 'none',
            rejectButtonStyleClass: 'p-button-text',
            accept: async () => {
                let data: any = {
                    is_active: false,
                };
                const response: AxiosResponse =
                    await this.patientListService.changeDoctor(data, 'id');
                this.getWard();
                // this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted' });
            },
            reject: () => {
                this.messageService.add({
                    severity: 'error',
                    summary: 'Rejected',
                    detail: 'You have rejected',
                    life: 3000,
                });
            },
        });
    }
    // confirm() {
    //     this.confirmationService.confirm({
    //         header: 'Confirmation',
    //         message: 'Please confirm to proceed moving forward.',
    //         acceptIcon: 'pi pi-check mr-2',
    //         rejectIcon: 'pi pi-times mr-2',
    //         rejectButtonStyleClass: 'p-button-sm',
    //         acceptButtonStyleClass: 'p-button-outlined p-button-sm',
    //         accept: () => {
    //             this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted', life: 3000 });
    //         },
    //         reject: () => {
    //             this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
    //         }
    //     });
    // }
    confirm() {
        this.confirmationService.confirm({
            header: 'Are you sure?',
            message: 'Please confirm to proceed.',
            accept: () => {
                this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted', life: 3000 });
            },
            reject: () => {
                this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
            }
        });
    }
    async deleteAdmit(event: Event) {
        console.log(event);
        let jsonEvent: any = event;
        this.confirmationService.confirm({
            target: event.target as EventTarget,
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            acceptButtonStyleClass: "p-button-danger p-button-text",
            rejectButtonStyleClass: "p-button-text p-button-text",
            acceptIcon: "none",
            rejectIcon: "none",

            accept: async () => {
                // this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'Record deleted' });
                let data: any = {
                    is_active: false,

                };
                const response: AxiosResponse = await this.patientListService.changeDoctor(jsonEvent.id,data);
                this.getWard();
            },
            reject: () => {
                this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'ยกเลิกกระบวนการ' });
            }
        });
    }
    async deleteAdmitn(datas: any) {
        console.log(datas);
        this.confirmationService.confirm({
            // target: event.target as EventTarget,
            message: 'คำเดือน คุณต้องการลบ จริงหรือไม่ ?',
            icon: 'pi pi-info-circle',
            acceptButtonStyleClass: 'p-button-danger p-button-sm',
            accept: () => {
                // this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'Record deleted', life: 3000 });
                let data: any = {
                    is_active: false,
                };
                // const response: AxiosResponse = await this.patientListService.changeDoctor( data, 'id' );
                //   this.getWard();
            },
            reject: () => {
                this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
            }
        });
    }


 
    navigateWaitingAdmit(data: any) {
        console.log(data);
        let jsonString = JSON.stringify(data);
        console.log(jsonString);
        this.router.navigate(['/waiting-admit'], { queryParams: { data: jsonString } });

    }
    navigateNurseNote(data: any) {
        console.log(data);
        let jsonString = JSON.stringify(data);
        console.log(jsonString);
        this.router.navigate(['/nurse-note'], { queryParams: { data: jsonString } });

    }
    navigateTest(data: any) {
        console.log(data);//data =  clinic id      
       
        this.router.navigate(['/nurse-note'], { queryParams: { data: 'clinicID' } });

    }
    navigateChangeWard(data: any) {
        console.log(data);
        let jsonString = JSON.stringify(data);
        console.log(jsonString);
        this.router.navigate(['/chang-ward'], { queryParams: { data: jsonString } });

    }
    onDateSelect(event:any){   
        if(event != null ){
            let date = new Date(event);
            let _time = date.getTime() + (7*60*60*1000);
            let search_date = new Date(_time).toISOString().split('T')[0];
            // console.log("search_date",search_date);
            this.dataSet = this.listPatients.filter(
            (x:any) => x.search_date === search_date
            );
        }else{

            this.dataSet = this.listPatients;
        }
      
    }
   
}
